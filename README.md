**Installation du projet**

- Faire un clone du projet
- se deplacer dans repertoire drupal
- exécuter la `commande composer install`
- ajouter ces lignes au fichier drupal/web/sites/default/settings.php
```
$databases['default']['default'] = array (
    'database' => getenv('MYSQL_DATABASE'),
    'username' => getenv('MYSQL_USER'),
    'password' => getenv('MYSQL_PASSWORD'),
    'prefix' => '',
    'host' => 'mysql',
    'port' => '3306',
    'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
    'driver' => 'mysql',
  );
```

- Ajouter cette valeur `RmCzoNBeu4G9wQzzbuuZpvORyxRZyoNc23vl6sMfLsBPbRmdez5bS76lBDkQ1u5O9LHx3jmKhg` à $settings['hash_salt'] si c'est vide.
- revenir à la racine du projet et exécuter `docker-compose up --build -d`
- accéder au site à l'adresse [http://localhost:81](http://localhost:81)
- login/mot de passe de l'administrateur: **admin/admin**
- Pour utiliser drush `docker exec -it -u apache dcd-php /var/www/localhost/vendor/bin/drush `